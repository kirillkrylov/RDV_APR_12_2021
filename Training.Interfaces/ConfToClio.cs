﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Terrasoft.Core;

namespace Training.Interfaces
{
	public interface IConfToClio
	{
		 void PostMessageToAll(string sender, string message);
		 void PostMessage(UserConnection userConnection, string sender, string message);
		void LogErrorMessage(string Message);
	}
}
