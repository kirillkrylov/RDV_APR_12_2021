# Введение в разработку на платформе Creatio c Кириллом Крыловым
С 12 Апреля 2021 по 15 Апреля 2021




## Полезные материалы
- [Развертывание on-site][razvertyvanie_onsite]
- [Файловый контент пакетов][rabota_s_faylovym_kontentom]
- [Внешние IDE][vneshnie_ide]
- [Разработка исходного кода в файловом контенте (пакет-проект)][clio]
- [Custom Logging with NLog][nlog]
- [Элемент процесса Задание-сценарий][element_processa_zadanije_scenarij]
- [Бизнес-логика объектовEventListener_Contact][sobytiynyy_sloy_obekta]

## Видео запись уроков
|День|Запись|
|:--:|:--:|
|1|[Запись][d1v]|
|2|[Запись][d2v]|
|3|[Запись][d3v]|
|4|[Запись][d4v]|

## **Форма обратной связи**
Пожалуйста пройдите опрос удовлетворенности обучения по **[ссылке][feedbackForm]**

GitIgnore - https://raw.githubusercontent.com/github/gitignore/master/VisualStudio.gitignore


<!-- Named Links Video Recordings-->
[d1v]:https://creatio-global.zoom.us/rec/play/UPScb05lCuuwuaJ3vIifnRJLl7mJ0mhsyerfimscmrMeQQi_pDpDcm34MVJneEu9MhtK01-dsnLWQn25.RdWO4gk916jjcxNd
[d2v]:https://creatio-global.zoom.us/rec/play/3xY7E3WTc8XtX1_8P5ldaQ6EGPxdpvNqwnwSfMmOZVeOV_-jd-INbcvfSW2LA3smBih4aPuPpU2JQKF0.-vA42pq3hOEEl5pX
[d3v]:https://creatio-global.zoom.us/rec/play/7lFg5Wzu0tNmhhpA8UT5Kq35rFW5AF6W13EbKptPlM7lfuTIS4tOs3B7Mwp63UdhjRahv_aR0LSxN29Y.qoj0jvfHSuzxp5XJ
[d4v]:https://creatio-global.zoom.us/rec/play/tYko15otb2Wh9Kwsh8v1wlPAHShiecr3c7tzYWnvAW8Z4U2OeaibOjoAC7w6MesnryBAVV3HuHbxMRG5.d9qQG22U88XcPpE9

<!-- Named Links Academy Materials -->
[razvertyvanie_onsite]:https://academy.terrasoft.ru/docs/user/ustanovka_i_administrirovanie/razvertyvanie_onsite
[rabota_s_faylovym_kontentom]:https://academy.terrasoft.ru/docs/developer/development_tools/packages/rabota_s_faylovym_kontentom
[vneshnie_ide]:https://academy.terrasoft.ru/docs/developer/development_tools/development_in_external_ide/vneshnie_ide
[clio]: https://academy.terrasoft.ru/docs/developer/back-end_development/project_package/razrabotka_iskhodnogo_koda_v_faylovom_kontente_%28paket-proekt%29

[nlog]: https://github.com/Academy-Creatio/TrainingProgramm/wiki/Custom-Logging-with-NLog
[element_processa_zadanije_scenarij]: https://academy.terrasoft.ru/docs/user/biznes_processy/spravka_po_elementam_processov/dejstviya_sistemy/element_processa_zadanije_scenarij

[sobytiynyy_sloy_obekta]: https://academy.terrasoft.ru/docs/developer/back-end_development/entity_event_layer/sobytiynyy_sloy_obekta


[feedbackForm]:https://forms.office.com/Pages/ResponsePage.aspx?id=-6Jce0OmhUOLOTaTQnDHFs1n4KjdfnVBtjvFqBN3Vk9UMldJOVZURlVZREVTRkFJT0w3UlpWMTRISy4u