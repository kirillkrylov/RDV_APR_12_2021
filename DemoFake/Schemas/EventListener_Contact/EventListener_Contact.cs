using Common.Logging;
using System;
using Terrasoft.Core;
using Terrasoft.Core.Entities;
using Terrasoft.Core.Entities.Events;

namespace Terrasoft.Configuration.Dev.Pkg.DemoFake.Schemas.EventListener_Contact
{
	/// <summary>
	/// Listener for 'EntityName' entity events.
	/// </summary>
	/// <seealso cref="Terrasoft.Core.Entities.Events.BaseEntityEventListener" />
	[EntityEventListener(SchemaName = "Contact")]
	class EventListener_Contact : BaseEntityEventListener
	{
		
		public override void OnSaving(object sender, EntityBeforeEventArgs e)
		{
			base.OnSaving(sender, e);
			Entity entity = (Entity)sender;
			//entity.GetColumnValue()
			var id = entity.GetTypedColumnValue<Guid>("Id");
			
			var newName = entity.GetTypedColumnValue<string>("Name");
			var oldName = entity.GetTypedOldColumnValue<string>("Name");
			
			var newEmail = entity.GetTypedColumnValue<string>("Email");
			var oldEmail = entity.GetTypedOldColumnValue<string>("Email");


			if (oldEmail.Contains("@creatio.com"))
			{
				ILog _log = LogManager.GetLogger("GuidedLearningLogger");
				e.IsCanceled = true;
				_log.Error($"Canceled onSaving because email contains @creatio.com for {id}");
			}

			UserConnection userConnection = entity.UserConnection;
			MsgChannelUtilities.PostMessage(userConnection, GetType().Name,
				"you are not allowed to modify contacts with email containing @creatio.com");


		}
		public override void OnSaved(object sender, EntityAfterEventArgs e)
		{
			base.OnSaved(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}

		public override void OnInserting(object sender, EntityBeforeEventArgs e)
		{
			base.OnInserting(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}
		public override void OnInserted(object sender, EntityAfterEventArgs e)
		{
			base.OnInserted(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}

		public override void OnUpdating(object sender, EntityBeforeEventArgs e)
		{
			base.OnUpdating(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}
		public override void OnUpdated(object sender, EntityAfterEventArgs e)
		{
			base.OnUpdated(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}

		public override void OnDeleting(object sender, EntityBeforeEventArgs e)
		{
			base.OnDeleting(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}
		public override void OnDeleted(object sender, EntityAfterEventArgs e)
		{
			base.OnDeleted(sender, e);
			Entity entity = (Entity)sender;
			UserConnection userConnection = entity.UserConnection;
		}

	
	}
}
