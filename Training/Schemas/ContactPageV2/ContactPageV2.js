define("ContactPageV2", ["ServiceHelper"], function(ServiceHelper) {
	return {
		entitySchemaName: "Contact",
		attributes: {
			"MyattributeOne": {
				dependencies: [
					{
						columns: ["MyString", "Name"],
						methodName: "onMyStringChange"
					},
					{
						columns: ["Email" ],
						methodName: "onEmailChange"
					}
				]
			},
			"Account": {
				lookupListConfig: {
					columns: ["Owner", "Country", "Country.Name"]
				}
			},
		},
		messages: {
			// Published on: ContactSectionV2.Training
			"SectionActionClicked": {
				mode: this.Terrasoft.MessageMode.PTP,
				direction: this.Terrasoft.MessageDirectionType.SUBSCRIBE
			}
		},
		modules: /**SCHEMA_MODULES*/{}/**SCHEMA_MODULES*/,
		details: /**SCHEMA_DETAILS*/{}/**SCHEMA_DETAILS*/,
		businessRules: /**SCHEMA_BUSINESS_RULES*/{}/**SCHEMA_BUSINESS_RULES*/,
		methods: {
			/**
			* @inheritdoc Terrasoft.BasePageV2#init
			* @overridden
			*/
			init: function() {
				this.callParent(arguments);
				this.subscribeToMessages();
			},
			/**
			* @inheritdoc Terrasoft.BasePageV2#onEntityInitialized
			*/
			onEntityInitialized: function() {
				this.callParent(arguments);
				//debugger;
			},
			onMyStringChange:function(){
				if(arguments && arguments[1]){
					var colChanged = arguments[1];
					var newValue = this.get(arguments[1]);
					this.showInformationDialog(colChanged + ': ' + newValue);
				}
			},
			onMyMainButtonClick: function(){
				var tagMainBtn = arguments[3];
			},
			onMySubButtonClick: function(){
				var tagMainBtn = arguments[0];
				
				this.doESQ();
			},
			/**
			 * @inheritdoc Terrasoft.BasePageV2#getActions
			 * @overridden
			 */
			getActions: function() {
				var actionMenuItems = this.callParent(arguments);
				actionMenuItems.addItem(this.getButtonMenuItem({Type: "Terrasoft.MenuSeparator",Caption: ""}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Page Action Item One",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item1",
					ImageConfig: this.get("Resources.Images.CreatioSquare"),
				}));
				actionMenuItems.addItem(this.getButtonMenuItem({
					"Caption": "Page Action Item Two",
					"Click": {bindTo: "onActionClick"},
					"Enabled": true,
					"Tag": "Item2"
				}));
				return actionMenuItems;
			},
			onActionClick: function(){
				let tag = arguments[0];

				//use tag to handle button clicks
				if(tag){
					this.showInformationDialog("Button with tag: "+tag+" clicked");
				}
			},

			subscribeToMessages: function(){
				this.sandbox.subscribe(
					"SectionActionClicked",
					function(){this.onSectionMessageReceived();},
					this, 
					["THIS_IS_MY_TAG2"]);
			},
			
			onSectionMessageReceived: function(){
				var id = this.$Id
				
				var yB = this.Terrasoft.MessageBoxButtons.YES;
				yB.style = "GREEN";
				
				var nB = this.Terrasoft.MessageBoxButtons.NO;
				nB.style = "RED";

				this.showConfirmationDialog(
					"ARE YOU SURE YOU WANT TO PROCEED ?",
					function (returnCode) {
						if (returnCode === this.Terrasoft.MessageBoxButtons.NO.returnCode) {
							return;
						}
						this.onAgeBtnClick();
					},
					[
						//this.Terrasoft.MessageBoxButtons.NO.returnCode,
						//this.Terrasoft.MessageBoxButtons.YES.returnCode
						yB.returnCode,
						nB.returnCode
					],
					null
				);
			},

			/** Вызов WebService */
			onAgeBtnClick: function() {
				var name = this.get("Name");
				// Object initializing incoming parameters for the service method.
				var serviceData = {
					// The property name corresponds to the incoming parameter name of the service method.
					"p" : {
						"NameAttribute": this.$Name,
						"Email" : this.$Email
					},
					"name" : "TestName"
				};
				// Calling the web service and processing the results.
				//https://[appName].domaincom/0/rest/ClassName/MethodName
				ServiceHelper.callService("DemoWebService", "PostMethodName",
					function(response) {
						//var result = response.GetContactIdByNameResult;
						this.showInformationDialog(response);
					}, serviceData, this);
			},

			/**Валидация поля */
			/**
			 * @inheritdoc Terrasoft.BaseSchemaViewModel#setValidationConfig
			 * @override
			 */
			 setValidationConfig: function() {
				this.callParent(arguments);
				this.addColumnValidator("MyString", this.myValidator);
				this.addColumnValidator("Email", this.myValidator);
			},
			myValidator: function() {
				var invalidMessage = "";
				var colUnderValidation = this.get(arguments[1].columnPath);
				if (colUnderValidation.length <10){
					invalidMessage = "Value is to short";
				}
				return {
					invalidMessage: invalidMessage
				};
			},

			doESQ: function(){
				var esq = this.Ext.create("Terrasoft.EntitySchemaQuery", {rootSchemaName: "Account"});
				esq.addColumn("Id");
				esq.addColumn("Name");
				esq.addColumn("Industry");
				esq.addColumn("AlternativeName");
				var i = 0;

				var esqFirstFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Country.Name", "Mexico");
				var esqSecondFilter = esq.createColumnFilterWithParameter(Terrasoft.ComparisonType.EQUAL, "Country.Id", "e0be1264-f36b-1410-fa98-00155d043204");
				
				esq.filters.logicalOperation = Terrasoft.LogicalOperatorType.OR;
				esq.filters.add("esqFirstFilter", esqFirstFilter);
				esq.filters.add("esqSecondFilter", esqSecondFilter);

				esq.getEntityCollection(
					function (result) {
						if (!result.success) {
							// error processing/logging, for example
							this.showInformationDialog("Data query error");
							return;
						}
						result.collection.each(
							function (item) {
								i++;
								var name = name + " "+item.$Name;
						});
						this.showInformationDialog("Total Accounts" + i);
					},
					this
				);
			}

		},
		dataModels: /**SCHEMA_DATA_MODELS*/{}/**SCHEMA_DATA_MODELS*/,
		diff: /**SCHEMA_DIFF*/[
			{
				"operation": "insert",
				"name": "MyString",
				"values": {
					"layout": {
						"colSpan": 12,
						"rowSpan": 1,
						"column": 6,
						"row": 3
					},
					"bindTo": "MyString",
					"tip": {
						"content": {
							"bindTo": "Resources.Strings.MyStringTip"
						}
					},
					"enabled": true
				},
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"index": 6
			},

			/** Buttons Example */
			{
				"operation": "insert",
				"parentName": "ContactGeneralInfoBlock",
				"propertyName": "items",
				"name": "MyRedButton",
				"values": {
					"layout": {
						"column": 13,
						"row": 4,
						"colSpan": 6
					},
					"itemType": this.Terrasoft.ViewItemType.BUTTON,
					"style": Terrasoft.controls.ButtonEnums.style.RED,
					"click": {"bindTo": "onMyMainButtonClick"},
					"tag": "red",
					"caption": {"bindTo": "Resources.Strings.MyRedBtnCaption"},
					"hint": {"bindTo": "Resources.Strings.MyRedBtnHint"},
					"menu": {
						"items": [
							{
								caption: "Sub Item 1",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 1 hint",
								tag: "subItem1"
							 },
							 {
								 caption: "Sub Item 2",
								 click: {bindTo: "onMySubButtonClick"},
								 visible: true,
								 hint: "Sub item 2 hint",
								 tag: "subItem2"
							  }
						],
					}
				}
			},
			
			{
				"operation": "insert",
				"parentName": "LeftContainer",
				"propertyName": "items",
				"name": "PrimaryContactButtonGreen",
				"values": {
					itemType: Terrasoft.ViewItemType.BUTTON,
					style: Terrasoft.controls.ButtonEnums.style.GREEN,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					markerValue: "GreenMarker",
					caption: "PAGE BTN",
					click: {bindTo: "onMyMainButtonClick"},
					tag: "green",
					menu: {
						"items": [
							{
								caption: "Sub Item 1",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 1 hint",
								tag: "subItem1"
							 },
							 {
								 caption: "Sub Item 2",
								 click: {bindTo: "onMySubButtonClick"},
								 visible: true,
								 hint: "Sub item 2 hint",
								 tag: "subItem2"
							  }
						],
					}
					
				}
			},
			{
				"operation": "insert",
				"parentName": "LeftContainer",
				"propertyName": "items",
				"name": "PrimaryContactButtonBlue",
				"values": {
					itemType: Terrasoft.ViewItemType.BUTTON,
					style: Terrasoft.controls.ButtonEnums.style.BLUE,
					classes: {
						"textClass": ["actions-button-margin-right"],
						"wrapperClass": ["actions-button-margin-right"]
					},
					caption: "PAGE BTN",
					click: {bindTo: "onMyMainButtonClick"},
					tag: "green",
					menu: {
						"items": [
							{
								caption: "Sub Item 1",
								click: {bindTo: "onMySubButtonClick"},
								visible: true,
								hint: "Sub item 1 hint",
								tag: "subItem1"
							 },
							 {
								 caption: "Sub Item 2",
								 click: {bindTo: "onMySubButtonClick"},
								 visible: true,
								 hint: "Sub item 2 hint",
								 tag: "subItem2"
							  }
						],
					}
					
				}
			}


		]/**SCHEMA_DIFF*/
	};
});
