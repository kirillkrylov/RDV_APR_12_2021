using global::Common.Logging;
using Terrasoft.Core;
using Terrasoft.Core.Factories;
using Training.Interfaces;

namespace Training
{

	[DefaultBinding(typeof(IConfToClio))]
	public class ConfToClio : IConfToClio
	{
		public void PostMessage(UserConnection userConnection, string senderName, string messageText)
		{

			Terrasoft.Configuration.MsgChannelUtilities.PostMessage(userConnection, senderName, messageText);


		}

		public void PostMessageToAll(string senderName, string messageText)
		{
			Terrasoft.Configuration.MsgChannelUtilities.PostMessageToAll(senderName, messageText);
		}




		public void LogErrorMessage(string Message)
		{
			ILog log = LogManager.GetLogger("GuidedLearningLogger");
			log.Error(Message);

		}
	}
} 